**Convolutional Neural Network**  ![CNN](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/CNN_model.png)

*A convolutional neural network is a neural network which is used for image processing applications like image classification,object detection,semantic segmentation, etc.*

*The architecture of CNN consists of convolution layers, max-pooling or average pooling layers, dropout and fully connected layers.*

> Convolution layer  ![convolution](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/convolution_layer.png)

*This layer is used to extract features from the previous layer by applying a number of filters (kernels). Initially we apply kernels on the input image which may consist of 1 or 3 channels and then consequently number of channels increases as the number of kernels increase thoughout the network.*

*Initially , features like edges,vertical,horizontal or curved lines are extracted and then these features are measured to extract more meaning when it goes deep in the network.*

*Relu activation function is applied after each convolution layer.*

> Filter  ![kernel](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/filter.png)

*A filter is a matrix used for extracting specific features from the previous layer by convolving it with a specific stride.*

> Hyperparameters  

*These are parameters which are used for increasing the accuracy of model.The hyperparameters mostly used for CNN model are stride,padding,etc.*

> Stride  ![stride](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/stride.png)

*Stride is a hyperparameter which is used to define a step value so that the filter skips the pixel value by that step value.*

> Padding  ![padding](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/padding.png)

*Padding is a hyperparameter which is used to preserve the dimension of the output after applying kernels on previous layer.We use zero padding so that features get extracted from every portion of the previous layer.*

> Max-pooling layer  ![max-pooling](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/max_pooling.jpeg)

*A max-pooling layer is a layer which is applied to the output of convolution layer ,that selects maximum pixel values such that dimensions get reduced so that more meaning gets extracted.*

> Average pooling layer  ![average pooling](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/Average-Pooling.png)

*An average pooling layer is a layer which is applied to the output of convolution layer, that selects the average value of pixel such that dimensions get reduced so that more meaning gets extracted.*

> Dropout layer  ![dropout](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/dropout.png)

*The function of dropout layer is to deactivate some neurons during training to avoid overfitting.*

> Fully connected layers  ![FCL](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/FCL.jpg)

*Fully connected layers in CNN consists of flatten layer and DNNS .*

> Flatten layer  ![flatten](https://gitlab.com/adityaashishx123/ai-user-training/-/blob/master/CNN%20images/flatten.png)

*This layer converts all pixel values from different feature maps into a single vector .*
